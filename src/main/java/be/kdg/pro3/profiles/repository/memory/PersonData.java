package be.kdg.pro3.profiles.repository.memory;

import be.kdg.pro3.profiles.domain.Person;

import java.util.List;

public class PersonData {
	static final List<Person> data = List.of(
			new Person(1,"JONES", "JACK","of all trades"),
			new Person(2,"POTTER", "JACK","Lilly's daddy"),
			new Person(3,"POTTER", "MIA","Lilly's mummy"),
			new Person(4,"REED", "Jack","Union")
		);
}
