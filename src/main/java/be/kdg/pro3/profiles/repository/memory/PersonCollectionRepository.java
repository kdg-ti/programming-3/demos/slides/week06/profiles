package be.kdg.pro3.profiles.repository.memory;

import be.kdg.pro3.profiles.domain.Person;
import be.kdg.pro3.profiles.repository.PersonRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.*;

import static java.util.stream.Collectors.toMap;

@Repository
@Profile("poc")
public class PersonCollectionRepository implements PersonRepository {

	private final Map<Integer, Person> data ;

	public PersonCollectionRepository() {
		data=PersonData.data.stream().collect(toMap(p -> p.getId(), p->p));
	}

	@Override
	public List<Person> findByName(String name) {
		return data.values().stream().filter(person -> person.getName().equals(name)).toList();
	}
}
