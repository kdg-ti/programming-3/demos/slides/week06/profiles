package be.kdg.pro3.profiles.repository.jdbcTemplate;

import be.kdg.pro3.profiles.domain.Person;
import be.kdg.pro3.profiles.repository.PersonRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;
import java.util.Map;

@Repository
@Profile("code-dev")
public class PersonJdbcTemplateRepository implements PersonRepository {

	private JdbcTemplate jdbcTemplate;

	public PersonJdbcTemplateRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<Person> findByName(String name) {
		return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
			(rs, rowNum) -> new Person(rs.getInt("id"),
				rs.getString("name"),
				rs.getString("firstname"),
				rs.getString("remark")),
			name);
	}




}
