package be.kdg.pro3.profiles.repository;


import be.kdg.pro3.profiles.domain.Person;

import java.util.List;

public interface PersonRepository {
	List<Person> findByName(String name);

}
