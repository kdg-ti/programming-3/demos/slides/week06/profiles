package be.kdg.pro3.profiles.repository.jdbc;

import be.kdg.pro3.profiles.domain.Person;
import be.kdg.pro3.profiles.exception.DatabaseException;
import be.kdg.pro3.profiles.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("prod")
public class PersonJdbcRepository implements PersonRepository {

	private String dbURL;
	private String user;
	private String password;

	public PersonJdbcRepository(@Value("${spring.datasource.url}") String dbURL,
		@Value("${spring.datasource.username:''}") String user,
		@Value("${spring.datasource.password:''}") String password) {
		this.dbURL = dbURL;
		this.user = user;
		this.password = password;
	}

	@Override
	public List<Person> findByName(String name) {
		try (Connection connection = DriverManager.getConnection(dbURL, user, password);
		     Statement statement = connection.createStatement();
		     ResultSet resultSet = statement.executeQuery("SELECT * FROM PERSONS")) {
			List<Person> result = new ArrayList<>();
			while (resultSet.next()) {
				result.add(new Person(resultSet.getInt("ID"),
					resultSet.getString("NAME"),
					resultSet.getString("FIRSTNAME"),
					resultSet.getString("REMARK")));
			}
			return result;
		} catch (SQLException e) {
			throw new DatabaseException("Cannot get all students", e);
		}
	}
}
